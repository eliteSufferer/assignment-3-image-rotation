#ifndef IMAGE_ROTATE_UTIL_H
#define IMAGE_ROTATE_UTIL_H

#include <stdbool.h>
#include <stdlib.h>

bool check_if_array_contains(const int* array, size_t size, int x);

#endif //IMAGE_ROTATE_UTIL_H
