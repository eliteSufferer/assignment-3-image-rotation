#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>


#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)


struct image {
    int32_t width, height;
    struct pixel* data;
};


void image_free(struct image* img);

#endif // IMAGE_H
