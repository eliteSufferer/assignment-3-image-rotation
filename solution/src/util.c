#include "../include/util.h"
bool check_if_array_contains(const int* array, size_t size, int x){
    for (size_t i = 0; i < size; ++i){
        if (array[i] == x){
            return true;
        }
    }
    return false;
}
