#include "../include/transform.h"
#include <stdlib.h>

struct image rotate(struct image const source, int angle) {
    struct image rotated;


    angle = ((angle % 360) + 360) % 360;

    if (angle == 90 || angle == 270) {
        rotated.width = source.height;
        rotated.height = source.width;
        rotated.data = malloc(rotated.width * rotated.height * sizeof(struct pixel));
        if (!rotated.data) {
            exit(1);
        }

        for (uint64_t y = 0; y < source.height; ++y) {
            for (uint64_t x = 0; x < source.width; ++x) {
                struct pixel current_pixel = source.data[y * source.width + x];
                if (angle == 90) {
                    rotated.data[(rotated.width - y - 1) + x * rotated.width] = current_pixel;
                } else {
                    rotated.data[y + (rotated.height - x - 1) * rotated.width] = current_pixel;
                }
            }
        }
    } else if (angle == 180) {
        rotated.width = source.width;
        rotated.height = source.height;
        rotated.data = malloc(rotated.width * rotated.height * sizeof(struct pixel));
        if (!rotated.data) {
            exit(1);
        }

        for (uint64_t y = 0; y < source.height; ++y) {
            for (uint64_t x = 0; x < source.width; ++x) {
                struct pixel current_pixel = source.data[y * source.width + x];
                rotated.data[(rotated.height - y - 1) * rotated.width + (rotated.width - x - 1)] = current_pixel;
            }
        }
    } else {
        return source;
    }

    return rotated;
}
