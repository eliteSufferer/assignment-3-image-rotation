#include "../include/bmp.h"
#include <stdlib.h>
#include <string.h>
#define BF_TYPE 0x4D42
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0

static size_t countPadding(int32_t width){
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}
enum read_status from_bmp(FILE* in, struct image* img) {

    if (!in || !img) {
        return READ_ERROR;
    }

    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BF_TYPE || header.biBitCount != BI_BIT_COUNT || header.biCompression != BI_COMPRESSION) {
        return READ_INVALID_SIGNATURE;
    }


    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data) {
        return READ_ERROR;
    }

    long padding_signed = (long) countPadding(img->width);

    size_t row = img->height - 1;
    for (uint32_t i = 0; i < img->height; ++i) {
        if (fread(&img->data[row * img->width], sizeof(struct pixel), img->width, in) != img->width) {
            free(img->data);
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding_signed, SEEK_CUR)) {
            free(img->data);
            return READ_ERROR;
        }
        --row;
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* img) {
    if (!out || !img) {
        return WRITE_ERROR;
    }

    struct bmp_header header;
    header.bfType = 0x4D42;
    header.bfileSize = 0;
    header.bfReserved = 0;
    header.bOffBits = sizeof(header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;


    size_t padding = countPadding(img->width);

    header.bfileSize = sizeof(header) + (img->width * sizeof(struct pixel) + padding) * img->height;

    header.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;


    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }


    uint8_t pad[3] = {0};
    for (uint32_t i = 0; i < img->height; ++i) {
        size_t row = img->height - 1 - i;
        if (fwrite(&img->data[row * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }

        if (padding > 0) {
            if (fwrite(pad, 1, padding, out) != padding) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}



enum read_status read_bmp_from_file(const char* filename, struct image* img) {
    FILE* in = fopen(filename, "rb");
    if (!in) {
        perror("Error opening input file");
        return READ_OPEN_ERROR;
    }

    enum read_status status = from_bmp(in, img);
    if (fclose(in) != 0){
        return READ_CLOSE_ERROR;
    }
    return status;
}

enum write_status write_bmp_to_file(const char* filename, const struct image* img) {
    FILE* out = fopen(filename, "wb");
    if (!out) {
        perror("Error accessing output file");
        return WRITE_OPEN_ERROR;
    }

    enum write_status status = to_bmp(out, img);

    if (fclose(out) != 0){
        return WRITE_CLOSE_ERROR;
    }

    return status;
}
