#include "../include/bmp.h"
#include "../include/transform.h"
#include "../include/util.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    int angles[] = {0, 90, 180, 270, -90, -180, -270};

    int size = sizeof(angles) / sizeof(angles[0]);

    if (!check_if_array_contains(angles, size, atoi(argv[3]))){
        printf("Incorrect angle");
        return 1;
    }

    const char* input_filename = argv[1];
    const char* output_filename = argv[2];
    int angle = atoi(argv[3]);

    struct image img;
    enum read_status rs = read_bmp_from_file(input_filename, &img);
    if (rs != READ_OK) {
        fprintf(stderr, "Error reading image from BMP file\n");
        return 1;
    }

    struct image rotated_img = rotate(img, angle);
    if (img.data != rotated_img.data) {
        image_free(&img);
    }

    enum write_status ws = write_bmp_to_file(output_filename, &rotated_img);
    if (ws != WRITE_OK) {
        fprintf(stderr, "Error writing image to BMP file\n");
        image_free(&rotated_img);
        return 1;
    }
    image_free(&rotated_img);

    return 0;
}
